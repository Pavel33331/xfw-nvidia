#ifdef _DEBUG
#define TOSTRING(x) #x
#define safe_func(res, obj, method, ...)   \
try {                                      \
    res = obj.attr(method)(##__VA_ARGS__); \
} catch(pybind11::error_already_set exc) { \
    res = pybind11::none();                \
    pybind11::print(                       \
        method,                            \
        "crashed with error",              \
        exc.what()                         \
    );                                     \
}
#define safe_proc(obj, method, ...) {           \
    pybind11::object res;                       \
    safe_func(res, obj, method, ##__VA_ARGS__); \
}
#else
#define safe_proc(obj, method, ...)      obj.attr(method)(##__VA_ARGS__);
#define safe_func(res, obj, method, ...) res = safe_proc(obj, method, ##__VA_ARGS__);
#endif
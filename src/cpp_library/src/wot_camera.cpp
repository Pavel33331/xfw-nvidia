/**
* This file is part of the XFW NVIDIA project.
* Copyright (c) 2018-2020 XVM Team.
*
* XFW NVIDIA is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* XFW NVIDIA is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "wot_camera.h"

void WotCamera::Enable()
{
    enableCamera();
    enableProjection();
}

void WotCamera::Disable()
{
    disableCamera();
    disableProjection();
}

ansel::Camera WotCamera::GetCameraSettings()
{
    ansel::Camera camera{};

    //projection
    camera.farPlane = pybind11::float_(_projection_original_object.attr("farPlane"));
    camera.nearPlane = pybind11::float_(_projection_original_object.attr("nearPlane"));
    camera.fov = static_cast<float>(pybind11::float_(_projection_original_object.attr("fov"))) * 180.0f / M_PI;

    //position and rotation
    pybind11::object o_camera, o_camera_matrix;
    safe_func(o_camera,        pybind11::module::import("BigWorld"), "camera");
    safe_func(o_camera_matrix, pybind11::module::import("Math"),     "Matrix", o_camera.attr("matrix"));

    auto [cam_position, cam_rotation] = bigworldToAnsel(o_camera_matrix);
    camera.position = cam_position;
    camera.rotation = cam_rotation;

    return camera;
}

void WotCamera::SetCameraSettings(ansel::Camera& cameraSettings)
{
    //projection
    _projection_original_object.attr("farPlane")  = cameraSettings.farPlane;
    _projection_original_object.attr("nearPlane") = cameraSettings.nearPlane;
    _projection_original_object.attr("fov")       = cameraSettings.fov * M_PI / 180.0f;

    //position and rotation
    pybind11::object o_camera;
    safe_func(o_camera, pybind11::module::import("BigWorld"), "camera");
    safe_proc(o_camera, "set", anselToBigworld(cameraSettings.position, cameraSettings.rotation));
}

void WotCamera::enableCamera()
{
    //import BigWorld
    pybind11::module m_bigworld = pybind11::module::import("BigWorld");

    //save old camera
    pybind11::object _camera_original_object;
    safe_func(_camera_original_object, m_bigworld, "camera");

    //create new camera
    pybind11::object free_camera;
    safe_func(free_camera, m_bigworld, "FreeCamera");
    safe_proc(free_camera, "set", _camera_original_object.attr("matrix"));

    //enable new camera
    safe_proc(m_bigworld, "camera", free_camera);
}

void WotCamera::enableProjection()
{
    //import BigWorld module
    pybind11::module m_bigworld = pybind11::module::import("BigWorld");

    //backup function and real object
    _projection_original_function = m_bigworld.attr("projection");
    _projection_original_object = _projection_original_function();

    //back initial values
    _projection_original_farPlane = pybind11::float_(_projection_original_object.attr("farPlane"));
    _projection_original_nearPlane = pybind11::float_(_projection_original_object.attr("nearPlane"));
    _projection_original_fov = pybind11::float_(_projection_original_object.attr("fov"));

    //hide projection object from others
    m_bigworld.attr("projection") = pybind11::module::import("XFW_NVIDIA").attr("ProjectionAccess");
}

void WotCamera::disableCamera()
{
    //restore camera
    safe_proc(pybind11::module::import("BigWorld"), "camera", _camera_original_object);
}

void WotCamera::disableProjection()
{
    //import BigWorld module
    pybind11::module m_bigworld = pybind11::module::import("BigWorld");

    //restore original projection function
    m_bigworld.attr("projection") = _projection_original_function;

    //restore original projection values
    pybind11::object o_projection;
    safe_func(o_projection, m_bigworld, "projection");
    
    o_projection.attr("farPlane") = _projection_original_farPlane;
    o_projection.attr("nearPlane") = _projection_original_nearPlane;
    o_projection.attr("fov") = _projection_original_fov;
}

std::pair<nv::Vec3, nv::Quat> WotCamera::bigworldToAnsel(pybind11::object& cameraMatrix)
{
    //get matrix
    XMMATRIX matrix{};
    for (auto row = 0; row < _matrix_dimension; row++) {
        for (auto col = 0; col < _matrix_dimension; col++) {
            pybind11::float_ matrix_val;
            safe_func(matrix_val, cameraMatrix, "get", row, col);
            matrix.r[row].m128_f32[col] = static_cast<float>(matrix_val);
        }
    }

    //calculate inverse matrix
    XMMATRIX matrix_inv = XMMatrixInverse(nullptr, matrix);

    //calculate quaternion
    XMVECTOR quat = XMQuaternionRotationMatrix(matrix_inv);

    return std::pair<nv::Vec3, nv::Quat>(
        { matrix_inv.r[3].m128_f32[0], matrix_inv.r[3].m128_f32[1], matrix_inv.r[3].m128_f32[2] },
        { quat.m128_f32[0], quat.m128_f32[1], quat.m128_f32[2], quat.m128_f32[3]});
}

pybind11::object WotCamera::anselToBigworld(nv::Vec3 position, nv::Quat rotation)
{
    //create transform matrix from position vector and rotation quaternion
    const XMMATRIX matrix_inv = XMMatrixInverse(nullptr, DirectX::XMMatrixAffineTransformation(
        XMVectorSet(1.0f, 1.0f, 1.0f, 1.0f),
        XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f),
        XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&rotation)),
        XMLoadFloat3(reinterpret_cast<const XMFLOAT3*>(&position))
    ));

    //create BigWorld matrix object
    pybind11::object o_matrix;
    safe_func(o_matrix, pybind11::module::import("Math"), "Matrix");
    for (auto row = 0; row < _matrix_dimension; row++) {
        for (auto col = 0; col < _matrix_dimension; col++) {
            safe_proc(o_matrix, "setElement", row, col, matrix_inv.r[row].m128_f32[col]);
        }
    }

    return o_matrix;
}

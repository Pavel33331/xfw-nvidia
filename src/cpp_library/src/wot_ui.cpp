/**
* This file is part of the XFW NVIDIA project.
* Copyright (c) 2018-2020 XVM Team.
*
* XFW NVIDIA is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* XFW NVIDIA is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "wot_ui.h"

void WotUi::SwitchVisibility(bool visibility)
{
    //get SF app
    pybind11::object o_i_apploader, o_apploader, o_app;
    safe_func(o_i_apploader, pybind11::module::import("skeletons.gui.app_loader"), "IAppLoader");
    safe_func(o_apploader,   pybind11::module::import("helpers.dependency"),       "instance", o_i_apploader);
    safe_func(o_app, o_apploader, "getApp");

    //set app visibility
    o_app.attr("component").attr("visible") = visibility;
    safe_proc(o_app.attr("graphicsOptimizationManager"), "switchOptimizationEnabled", visibility);

    //fire game/guiVisibility event to change crosshairs visibility
    pybind11::object o_game_event;
    safe_func(o_game_event, pybind11::module::import("gui.shared.events"), "GameEvent", 
        "game/guiVisibility",
        std::map<std::string, bool>{ {"visible", visibility } }
    );
    safe_proc(pybind11::module::import("gui.shared").attr("g_eventBus"), "handleEvent", o_game_event, 3); //3 is for battle
}

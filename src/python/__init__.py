"""
This file is part of the XFW.NVIDIA project.

Copyright (c) 2018-2020 XVM Team.

XFW.NVIDIA is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, version 3.

XFW.NVIDIA is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

#cpython
import logging
import platform

#wot client
from helpers import dependency
from skeletons.connection_mgr import IConnectionManager

#xfw.loader
import xfw_loader.python as loader

g_xfwnvidia = None

class XFWNVidia(object):
    def __init__(self):
        self.__native = None
        self.__initialized = False
        self.__package_name = 'com.modxvm.xfw.nvidia'

        try:
            xfwnative = loader.get_mod_module('com.modxvm.xfw.native')
            if not xfwnative:
                logging.error('[XFW/NVIDIA] Failed to load native module. XFW Native is not available')
                return

            if not xfwnative.unpack_native(self.__package_name):
                logging.error('[XFW/NVIDIA] Failed to load native module. Failed to unpack native module')
                return

            self.__native = xfwnative.load_native(self.__package_name, 'xfw_nvidia.pyd', 'XFW_NVIDIA')
            if not self.__native:
                logging.error("[XFW/NVIDIA] Failed to load native module. Crash report were not enabled")
                return

            self.__initialized = True

        except Exception:
            logging.exception("[XFW/HiDPI] Error when loading native library:")

    def is_initialized(self):
        return self.__initialized

    def ansel_init(self):
        try:
            self.__native.ansel_init()

            connection_manager = dependency.instance(IConnectionManager)
            connection_manager.onDisconnected += self.__on_disconnected

        except Exception:
            logging.exception("[XFW/NVIDIA][ansel_init]")

    def __on_disconnected(self):
            try:
                self.ansel_session_stop()
            except Exception:
                logging.exception("[XFW/NVIDIA][on_disconnected]")

    def ansel_session_start(self):
        try:
            self.__native.ansel_session_start()
        except Exception:
            logging.exception("[XFW/NVIDIA][ansel_session_start]")

    def ansel_session_stop(self):
        try:
            self.__native.ansel_session_stop()
        except Exception:
            logging.exception("[XFW/NVIDIA][ansel_session_stop]")

    def ansel_session_isrunning(self):
        try:
            return self.__native.ansel_session_isrunning()
        except Exception:
            logging.exception("[XFW/NVIDIA][ansel_session_isrunning]")
            return False

def xfw_is_module_loaded():
    if not g_xfwnvidia:
        return False

    return g_xfwnvidia.is_initialized()

g_xfwnvidia = XFWNVidia()
g_xfwnvidia.ansel_init()

# This file is part of the XFW NVIDIA project.
#
# Copyright (c) 2017-2020 XVM Team.
#
# XFW NVIDIA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, version 3.
#
# XFW NVIDIA is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

Push-Location $PSScriptRoot
$root = (Get-Location).Path -replace "\\","/"

Import-Module ./build_lib/library.psm1 -Force -DisableNameChecking

function Download-DevelPackage()
{
    Invoke-WebRequest "https://ci.appveyor.com/api/buildjobs/u01ok915a1tfiwur/artifacts/~output%2Fdeploy%2Fcom.modxvm.xfw.native_1.5.7-devel.zip" -OutFile devel.zip
    Expand-Archive -Path ./devel.zip -DestinationPath "$root/output/xfw_devel/"
    Remove-Item -Path "./devel.zip"
}

function Build-CmakeProject ($Name, $Config = "RelWithDebInfo", $Arch = "Win32", $Defines = $null)
{
    Write-Output "  * $Name"

    Remove-Item -Recurse -Path ./output/build/$Name/ -ErrorAction SilentlyContinue | Out-Null
    New-Item -ItemType Directory -Path ./output/build/$Name/ | Out-Null

    $root = (Get-Location).Path -replace "\\","/"

    Push-Location "$root/output/build/$Name/"

    cmake -T v141_xp -A $Arch "$root/src/$Name/" -DCMAKE_INSTALL_PREFIX="$root/output/component_cpp/$Arch" -DCMAKE_PREFIX_PATH="$root/output/xfw_devel/$Arch" $Defines
    if ($LastExitCode -ne 0) {
        Pop-Location
        exit $LastExitCode
    }

    cmake --build . --target INSTALL --config RelWithDebInfo
    if ($LastExitCode -ne 0) {
        Pop-Location
        exit $LastExitCode
    }

    Pop-Location
}

function Build-Python()
{
    Build-PythonFile -FilePath "./src/python/__empty__.py" -OutputDirectory "./output/component_python/res/mods/xfw_packages/xfw_nvidia/" -OutputFileName "__init__.pyc"
    Build-PythonFile -FilePath "./src/python/__init__.py"  -OutputDirectory "./output/component_python/res/mods/xfw_packages/xfw_nvidia/python/"
}

function Build-Deploy()
{
    $version = Get-Content "./src/meta/version.txt"

    Copy-Item -Path "./output/component_python/res/" -Destination "./output/wotmod/res" -Force -Recurse
    New-Item -Path "./output/wotmod/res/mods/xfw_packages/xfw_nvidia/native_32bit/" -ItemType Directory -ErrorAction SilentlyContinue | Out-Null

    Copy-Item -Path "./src/cpp_anselsdk/redist/AnselSDK32.dll" -Destination "./output/wotmod/res/mods/xfw_packages/xfw_nvidia/native_32bit/" -Recurse
    Copy-Item -Path "./src/cpp_anselsdk/redist/AnselSDK64.dll" -Destination "./output/wotmod/res/mods/xfw_packages/xfw_nvidia/native_64bit/" -Recurse

    Copy-Item -Path "./output/component_cpp/Win32/*.pyd" -Destination "./output/wotmod/res/mods/xfw_packages/xfw_nvidia/native_32bit/" -Recurse
    Copy-Item -Path "./output/component_cpp/x64/*.pyd"   -Destination "./output/wotmod/res/mods/xfw_packages/xfw_nvidia/native_64bit/" -Recurse

    Copy-Item -Path "./LICENSE.md"                       -Destination "./output/wotmod/LICENSE.md"

    (Get-Content "src/meta/wotmod_meta.xml.in").Replace("{{VERSION}}","${version}") | Set-Content "./output/wotmod/meta.xml"
    (Get-Content "src/meta/xfw_package.json").Replace("{{VERSION}}","${version}")  | Set-Content "./output/wotmod/res/mods/xfw_packages/xfw_nvidia/xfw_package.json"

    Create-Zip -Directory "./output/wotmod/"

    Move-Item "./output/wotmod/output.zip" "./output/deploy/mods/VER/com.modxvm.xfw.nvidia_${version}.wotmod" -Force
}

Remove-Item -Path ./output/  -Force -Recurse -ErrorAction SilentlyContinue
New-Item -ErrorAction SilentlyContinue -ItemType Directory -Path ./output/build/     | Out-Null
New-Item -ErrorAction SilentlyContinue -ItemType Directory -Path ./output/logs/      | Out-Null
New-Item -ErrorAction SilentlyContinue -ItemType Directory -Path ./output/deploy/    | Out-Null
New-Item -ErrorAction SilentlyContinue -ItemType Directory -Path ./output/deploy/mods/VER/ | Out-Null
New-Item -ErrorAction SilentlyContinue -ItemType Directory -Path ./output/xfw_devel/ | Out-Null

Write-Output "Downloading XFW.Native devel package"
Download-DevelPackage
Write-Output ""

Write-Output "Building python"
Build-Python
Write-Output ""

Write-Output "Building project"
Build-CmakeProject -Name cpp_library -Arch Win32
Build-CmakeProject -Name cpp_library -Arch x64
Write-Output ""

Write-Output "Building archives"
Build-Deploy
Write-Output ""

Pop-Location
